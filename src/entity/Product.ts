import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Product { //Entity ชื่อ Product
    @PrimaryGeneratedColumn({name: "product_id"}) //การทำแบบนี้จะทำให้ใน database เก็บ field นี้ที่่มีชื่อว่า product_id
    id: number;

    @Column({name: "product_name"})
    name: string;

    @Column({name: "product_price"})
    price: number
}